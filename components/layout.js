import styles from "../styles/layout.module.scss";

const Layout = ({ children }) => {
  return <div data-testid="child-holder" className={styles.main}>{children}</div>;
};

export default Layout;
