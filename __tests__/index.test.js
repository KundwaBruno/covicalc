/* eslint-disable max-lines-per-function */
/* eslint-disable no-unused-vars */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { act, fireEvent, render } from '@testing-library/react';
import Nav from '../components/nav';
import Layout from '../components/layout';
import { LandingPage } from "../pages/index";
import { mockData } from "../utils/mockData";

describe('Testing components',() => {
  it('should display render the navigation bar', async () => {
    const { getByText } = render(<Nav />);
    expect(getByText('COVICALC')).toBeInTheDocument();
  });
  it('should render the layout component', () => {
    const { getByTestId } = render(<Layout />);
    expect(getByTestId('child-holder')).toBeInTheDocument();
  });
});


describe('Index page', () => {
  it('should render the homepage with initial data', async () => {
    const { getByText } = render(<LandingPage initialData={mockData} />);
    fireEvent.click(getByText('SUBMIT'));
    await expect(getByText('22.2K')).toBeInTheDocument();
  });
});
