<!-- ABOUT THE PROJECT -->

<img src="./public/screenshot.png">

## Project name

Covidcalc

## Project description

Covidcalc is a web application that helps its users to generate data on COVID-19 updates.

### Built With

* [NextJs](https://nextjs.org/)
* [Sass](https://sass-lang.com/)
* [Javascript ES6](https://javascript.com)

### Resources

* [Covid API](https://documenter.getpostman.com/view/11144369/Szf6Z9B3?version=latest)

<!-- GETTING STARTED -->
## Getting Started

Read the following below steps to help you get covidcalc up and running

### Prerequisites

Install the required technologie(s)

* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/KundwaBruno/covicalc
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

## Usage

1. Enter all the required fields ( Country name and Date ) and hit the `submit` button as shown below:

<img src="./public/search.png">

2. The results should be displayed automatically:

<img src="./public/result.png">

## Contributors
* [Kundwa Bruno M.](https://www.itskbm.com/)
* [Awesomity lab](https://awesomity.rw/)